import React from "react";
import Banner from "./banner";
import HouseList from "./houseList";

const App = () => {
  return (
  <>
    <Banner></Banner>
    <HouseList></HouseList>
  </>
  );
};

export default App;
