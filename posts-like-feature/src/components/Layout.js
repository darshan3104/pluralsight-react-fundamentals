import React, { useContext } from "react";
import { ThemeContext, ThemeProvider } from "../contexts/ThemeContext";

const Layout = ({ startTheme, children }) => {
  return (
    <ThemeProvider startTheme={startTheme}>
      <LayoutNoThemeProvider startTheme={startTheme}>
        {children}
      </LayoutNoThemeProvider>
    </ThemeProvider>
  );
};

const LayoutNoThemeProvider = ({ children }) => {
  const { theme } = useContext(ThemeContext);
  return (
    <div
      className={
        theme === "light" ? "container-fluid light" : "container-fluid dark"
      }
    >
      {children}
    </div>
  );
};

export default Layout;
