import React, {useState}  from 'react'

const useTheme = (startTheme = "light") => {
    const [theme, setTheme] = useState(startTheme);

    const validateTheme = (themeValue) => {
        if (themeValue === "dark") {
            setTheme("dark")
        }else{
            setTheme("light")
        }
    }

  return {
    theme,
    setTheme: validateTheme
  }
}

export default useTheme